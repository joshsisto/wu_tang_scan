
import json
import datetime
import time
import requests


def convert_time(time_string):
    return time.ctime(time_string)


weather_url = 'https://api.darksky.net/forecast/cd562f431296f113d3a618a7ea1d94ef/38.561936,-121.423951'
air_url = 'http://www.airnowapi.org/aq/observation/zipCode/current/?format=application/json&zipCode=95817&distance=1&API_KEY=F62EEA9E-1177-456C-89B2-94CA763972C6'


def get_json(url):
    resp = requests.get(url)
    data = resp.json()
    return data


def get_aqi():
    air_json = get_json(air_url)
    aqi = air_json[0]['AQI']
    air_status = air_json[0]['Category']['Name']
    air_list = []
    air_list.append(aqi)
    air_list.append(air_status)
    return air_list

# print(get_aqi())


def get_weather():
    """ Returns a list of the weather
    [Temp, ] """
    data = get_json(weather_url)
    current_weather = data['currently']
    daily_sum = current_weather['summary']
    current_temp = current_weather['temperature']
    forecast = data['daily']['summary']
    weather_list = []
    weather_list.append(current_temp)
    weather_list.append(daily_sum)
    weather_list.append(forecast)
    return weather_list

# print(get_weather())
